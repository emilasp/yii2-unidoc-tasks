<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m170128_214853_new_tables_task_result__metrics__goals extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('unidoc_tasks_result', [
            'id'         => $this->primaryKey(11),
            'task_id'    => $this->integer(11),
            'accumulate' => $this->integer(11)->defaultValue(0),
            'result'     => $this->text(),
            'status'     => $this->smallInteger(1)->notNull(),// 0 - fail, 1 - success
            'created_at' => $this->dateTime(),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_unidoc_tasks_result_task_id',
            'unidoc_tasks_result',
            'task_id',
            'unidoc_tasks_task',
            'id'
        );

        /**
         * {"result_micro":100, "result_macro":1000, "result_actual":102, "result_expect": 300}
         * result_micro - за раз (2 статьи в день)
         * result_macro - всего(максимум возможного) - собственно ставим цель
         * result_actual - текущий результат
         * result_expect - ещё ожидается
         *
         * прогресс мы считаем так: текущий результат/(всего-ещё ожидается)
         */
        $this->addColumn('unidoc_tasks_task', 'metric', 'jsonb NULL DEFAULT \'[]\'');

        /**
         * По завершению(ям) оставить тезисы
         */
        $this->addColumn('unidoc_tasks_task', 'is_argue', $this->smallInteger(1)->null()); //

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('unidoc_tasks_result');
        $this->dropColumn('unidoc_tasks_task', 'metric');
        $this->dropColumn('unidoc_tasks_task', 'is_argue');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
