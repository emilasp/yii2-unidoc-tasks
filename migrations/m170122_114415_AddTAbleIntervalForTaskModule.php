<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m170122_114415_AddTAbleIntervalForTaskModule extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('unidoc_tasks_period', [
            'id'           => $this->primaryKey(11),
            'month_days'   => 'jsonb NULL DEFAULT \'[]\'',
            'week_days'    => 'jsonb NULL DEFAULT \'[]\'',
            'custom_dates' => 'jsonb NULL DEFAULT \'[]\'',
            'object'       => $this->string(128)->notNull(),
            'object_id'    => $this->integer(11)->notNull(),
            'created_at'   => $this->dateTime(),
            'created_by'   => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_unidoc_tasks_period_created_by',
            'unidoc_tasks_period',
            'created_by',
            'users_user',
            'id'
        );

        $this->createIndex('idx_unidoc_tasks_period_object', 'unidoc_tasks_period', ['object', 'object_id']);
        $sql = <<<SQL
            CREATE INDEX idx_unidoc_tasks_period_month_days ON unidoc_tasks_period USING gin(month_days);
SQL;

        $this->db->createCommand($sql)->execute();
        $sql = <<<SQL
            CREATE INDEX idx_unidoc_tasks_period_week_days ON unidoc_tasks_period USING gin(week_days);
SQL;

        $this->db->createCommand($sql)->execute();
        $sql = <<<SQL
            CREATE INDEX idx_unidoc_tasks_period_custom_dates ON unidoc_tasks_period USING gin(custom_dates);
SQL;

        $this->db->createCommand($sql)->execute();

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('unidoc_tasks_period');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
