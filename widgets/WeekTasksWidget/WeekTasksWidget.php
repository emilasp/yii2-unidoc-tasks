<?php

namespace emilasp\tasks\widgets\WeekTasksWidget;

use emilasp\core\components\base\Widget;
use emilasp\tasks\behaviors\TaskGoallBehavior;
use emilasp\tasks\models\forms\TaskResultModel;
use emilasp\tasks\models\Task;
use emilasp\unidoc\models\TaskResult;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Виджет формирует выбор дней недели, дней месяца и кастомных дат
 *
 * http://tutorialzine.com/2010/01/sticky-notes-ajax-php-jquery/
 *
 * Class StickyNotesWidget
 * @package emilasp\tasks\widgets\WeekTasksWidget
 */
class WeekTasksWidget extends Widget
{
    const TYPE_WEEK_DAYS    = 'week-days';
    const TYPE_MONTH_DAYS   = 'month-days';
    const TYPE_CUSTOM_DATES = 'custom-dates';

    const FORMAT_WEEK       = 1;
    const FORMAT_THREE_DAYS = 2;
    const FORMAT_ONE_DAY    = 3;

    public $type = self::TYPE_MONTH_DAYS;

    public $formatGrid = self::FORMAT_WEEK;

    private $tasks;

    private $model;
    private $date;

    /**
     * INIT
     */
    public function init()
    {
        parent::init();

        $this->registerAssets();
        $this->setFormatGrid();
        $this->setDate();
        $this->setTasks();

        $this->setRequestData();
    }

    /**
     * RUN
     */
    public function run()
    {
        $highTasks = $this->getTasks([
            'status'   => [Task::STATUS_NEW, Task::STATUS_ANALIZE, Task::STATUS_JOB],
            'priority' => Task::PRIORITY_HIGHTEST
        ]);
        $frogTasks = $this->getTasks([
            'status' => [Task::STATUS_NEW, Task::STATUS_ANALIZE, Task::STATUS_JOB],
            'type'   => Task::TYPE_FROG
        ]);
        $bayTasks  = $this->getTasks([
            'AND',
            ['status' => [Task::STATUS_NEW, Task::STATUS_ANALIZE, Task::STATUS_JOB]],
            ['>', 'price', 0]
        ]);
        $jobTasks  = $this->getTasks(['status' => Task::STATUS_JOB]);
        $openTasks = $this->getTasks(['NOT IN', 'status', [Task::STATUS_CANCEL, Task::STATUS_FINISH]], 'id DESC');

        echo $this->render('widget', [
            'tasksDays'    => $this->tasks,
            'highestTasks' => $highTasks,
            'bayTasks'     => $bayTasks,
            'frogTasks'    => $frogTasks,
            'jobTasks'     => $jobTasks,
            'openTasks'    => $openTasks,
            'model'        => $this->model,
            'formatGrid'   => $this->formatGrid,
            'curDate'      => $this->getDate(),
            'prevDate'     => $this->getDate('prev'),
            'nextDate'     => $this->getDate('next'),
        ]);
    }

    /**
     * Устанавливаем нужную структуру для задач за день
     *
     * @param string $dateDay Строка дата
     *
     * @return void
     */
    public function setTaskByDay(string $dateDay): void
    {
        $tasks = TaskGoallBehavior::getTasksByDate($dateDay, Task::TASK_TYPE_TASK);

        $this->tasks[$dateDay] = [];

        foreach ($tasks as $task) {
            if ($time = $task->time) {
                $time                           = date('H:00:00', strtotime($task->time));
                $times[$dateDay][$time]         = [];
                $this->tasks[$dateDay][$time][] = $task;
            } else {
                $times[$dateDay]['allDay']         = [];
                $this->tasks[$dateDay]['allDay'][] = $task;
            }
        }
    }

    /**
     * Получаем bootstrap класс для задачи ппо приоритету
     *
     * @param Task $task
     * @return string
     */
    public static function getTaskPriorityClass(Task $task): string
    {
        $class = ' btn-default';
        switch ($task->priority) {
            case Task::PRIORITY_HIGHTEST:
                $class = ' btn-danger';
                break;
            case Task::PRIORITY_HIGHT:
                $class = ' btn-warning';
                break;
            case Task::PRIORITY_MIDDLE:
                $class = ' btn-info';
                break;
        }

        return $class;
    }

    /**
     * Формируем полное наименование задачи для вывода в виджете
     *
     * @param Task $task
     * @param string $date
     * @return string
     */
    public static function getTaskFullName(Task $task, string $date) : string
    {
        $name = Html::tag('small', date('H:i', strtotime($task->time)) . ' ', ['class' => 'text-muted']);
        $name .= $task->name;

        $fails = $task->getDayResultsCount($date, 'fail');
        $badge = Html::tag(
            'span',
            $task->getDayResultsCount($date, 'good') . '/'
            . Html::tag('span', $fails, ['class' => ($fails ? 'text-danger' : '')]),
            ['class' => 'badge badge-right']
        );

        return $name . $badge;
    }

    /**
     * Устанавливливаем задачи на неделю
     */
    private function setTasks()
    {
        $dateStart = $this->getDate();
        $date      = new \DateTime($dateStart);

        $countDays = 1;
        if ($this->formatGrid === self::FORMAT_WEEK) {
            $countDays = 7;
            $date->modify('last Monday');
        } elseif ($this->formatGrid === self::FORMAT_THREE_DAYS) {
            $countDays = 3;
            $date->modify('-1 days');
        }

        $times = [];
        for ($i = 0; $i < $countDays; $i++) {
            $dateDay = $date->format('Y-m-d');

            $this->setTaskByDay($dateDay);

            $date->modify('+1 day');
        }

        $this->tasks = ArrayHelper::merge($times, $this->tasks);
        ksort($this->tasks);
    }

    /**
     * Registers the needed assets
     */
    private function registerAssets(): void
    {
        WeekTasksWidgetAsset::register($this->view);
    }

    /**
     * Формируем модель
     */
    private function getModel(): void
    {
        if (!$this->model) {
            $this->model = new TaskResultModel([
                'task_id' => Yii::$app->request->get('task_id'),
                'date'    => Yii::$app->request->get('date'),
                //'result'  => Yii::$app->request->get('result'),
            ]);
        }
    }

    /**
     * Заполняем пришедшими данными и сохраняем модель
     */
    private function setRequestData()
    {
        $this->getModel();

        if ($this->model->load(Yii::$app->request->post())) {
            $this->model->fillModel();
            if ($this->model->validate()) {
                $this->model->save();
            }
        }
    }

    /**
     * Устанавливаем тип грида(сохраняем/берем из сессии)
     */
    private function setFormatGrid()
    {
        if ($formatGrid = Yii::$app->request->post('formatGrid')) {
            Yii::$app->session->set('formatGrid', (int)$formatGrid);
        }

        $this->formatGrid = Yii::$app->session->get('formatGrid', self::FORMAT_WEEK);
    }

    /**
     * Устанавливаем дату
     */
    private function setDate()
    {
        $this->date = Yii::$app->request->get('date', date('Y-m-d'));
    }

    /**
     * Устанавливаем дату для формирования грида
     *
     * @param string|null $type
     * @return string
     */
    private function getDate(string $type = null): string
    {
        $countDays = 1;
        if ($this->formatGrid === self::FORMAT_WEEK) {
            $countDays = 7;
        } elseif ($this->formatGrid === self::FORMAT_THREE_DAYS) {
            $countDays = 3;
        }

        if ($type === 'prev') {
            return date('Y-m-d', strtotime($this->date . ' -' . $countDays . ' days'));
        }

        if ($type === 'next') {
            return date('Y-m-d', strtotime($this->date . ' +' . $countDays . ' days'));
        }

        return $this->date;
    }

    /**
     * Получаем задачи по условию
     *
     * @param array $condition
     * @return array|\yii\db\ActiveRecord[]
     */
    private function getTasks($condition, $order = null)
    {
        $query = Task::find()->byCreatedBy()->andWhere($condition);

        if ($order) {
            $query->orderBy($order);
        }
        return $query->all();
    }
}
