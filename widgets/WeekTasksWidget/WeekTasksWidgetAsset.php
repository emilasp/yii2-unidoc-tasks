<?php
namespace emilasp\tasks\widgets\WeekTasksWidget;

use yii\web\AssetBundle;

/**
 * Class WeekTasksWidgetAsset
 * @package emilasp\tasks\widgets\WeekTasksWidget
 */
class WeekTasksWidgetAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $js  = ['week-tasks.js'];
    public $css = ['week-tasks.css'];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
    ];
}
