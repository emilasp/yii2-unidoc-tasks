$('body').on('click', '.week-tasks-task', function (event) {
    var weekContainer = $('.week-tasks-week-container');
    var taskForm      = $('.week-tasks-form-container');
    var taskId        = $(this).data('id');
    var date          = $(this).data('date');

    weekContainer.toggle("slide", {direction: "left"}, 1000);
    taskForm.toggle("slide", {direction: "left"}, 1000);


    $.pjax({
        container: "#resultForm",
        "timeout": 0,
        push: false,
        "data": {"task_id": taskId, "date": date}
    });
});


$('body').on('click', '.btn-taskform-back', function (event) {
    var weekContainer = $('.week-tasks-week-container');
    var taskForm      = $('.week-tasks-form-container');

    weekContainer.toggle("slide", {direction: "left"}, 1000);
    taskForm.toggle("slide", {direction: "left"}, 1000);
});


$('body').on('beforeSubmit', '#task-week-form', function (event, jqXHR, settings) {
    var form = $(this);

    $('.week-tasks-form-container').data('sended', 1);

    $.pjax({
        method: 'POST',
        container: "#resultForm",
        "timeout": 0,
        push: false,
        "data": form.serialize()
    });

    return false;
});

$('body').on("pjax:end", "#resultForm", function () {
    var form   = $('#task-week-form');
    var sended = $('.week-tasks-form-container').data('sended');

    console.log(sended);

    if (sended && form.find('.has-error').length == 0) {
        $('.btn-taskform-back').click();
        $('.week-tasks-form-container').data('sended', 0);

        $.pjax({
            method: 'POST',
            container: "#tasks-week-table",
            "timeout": 0,
            push: false
        });

    }
});

$('body').on("click", ".form-result-expand", function () {
    var buttonIcon = $(this).find('.fa');
    $('.form-result-colapse').slideToggle('fast');


    if (buttonIcon.hasClass('fa-plus-square')) {
        buttonIcon.removeClass('fa-plus-square').addClass('fa-minus-square');
    } else {
        buttonIcon.removeClass('fa-minus-square').addClass('fa-plus-square');
    }
});

$('body').on("click", ".btn-send-result", function () {
    var button = $(this);
    var value  = button.hasClass('btn-danger') ? 11 : 9;

    $('.form-result-status').val(value);
    $('#task-week-form').submit();
});

$('body').on("click", ".btn-formatGrid button", function () {
    var button = $(this);
    var format  = button.data('format');

    $.pjax({
        method: 'POST',
        container: "#tasks-week-table",
        "timeout": 0,
        push: false,
        data:{formatGrid:format}
    });
});
