<?php
use emilasp\core\extensions\CodemirrorWidget\CodemirrorWidget;
use emilasp\media\extensions\FileInputWidget\FileInputWidget;
use emilasp\tasks\behaviors\TaskGoallBehavior;
use emilasp\unidoc\models\TaskResult;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Markdown;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

?>


<div class="week-tasks-form-container" style="display: none;">

    <?php Pjax::begin(['id' => 'resultForm']); ?>

    <h1><?= $model->task ? $model->task->name : '' ?></h1>

    <div class="row">
        <div class="col-md-6">
            <?= Html::button(
                Html::tag('i', '', ['class' => 'fa fa-backward ']) . ' ' . Yii::t('site', 'Back'),
                ['class' => 'btn btn-primary btn-taskform-back']
            ) ?>
        </div>
        <div class="col-md-6 text-right">
            <?= Html::a(
                Html::tag('i', '', ['class' => 'fa fa-pencil-square ']) . ' ' . Yii::t('site', 'Update'),
                ['/tasks/task/update', 'id' => $model->task_id],
                ['class' => 'btn btn-warning']
            ) ?>
        </div>
    </div>


    <?php $form = ActiveForm::begin([
        'id'                     => 'task-week-form',
        'fieldConfig'            => ['autoPlaceholder' => false],
        'formConfig'             => ['deviceSize' => 'sm'],
        'enableAjaxValidation'   => false,
        'enableClientValidation' => false,
    ]); ?>

    <?= $form->errorSummary($model, ['header' => '']); ?>

    <?= $form->field($model, 'task_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'is_argue')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'status')->hiddenInput(['class' => 'form-result-status'])->label(false) ?>


    <div class="row">
        <div class="col-md-12 <?= $curDate !== date('Y-m-d') ? 'hidden' : '' ?>">

            <?php if ($model->task && $model->task->resultType == TaskGoallBehavior::RESULT_TYPE_ACCUMULATION) : ?>
                <?= $form->field($model, 'accumulate')->input('number')->label() ?>
            <?php else: ?>
                <?= $form->field($model, 'accumulate')->hiddenInput()->label(false) ?>
            <?php endif; ?>

            <?php if (!$model->is_argue) : ?>
                <div class="form-result-expand">
                    <?= Html::button(
                        Yii::t('tasks', 'Add result argue') . ' ' . Html::tag('i', '',
                            ['class' => 'fa fa-plus-square']),
                        ['class' => 'btn btn-info btn-xs']
                    ) ?>
                </div>
            <?php endif; ?>

            <div class="row form-result-colapse" style="<?= !$model->is_argue ? 'display:none;' : '' ?>">
                <div class="col-md-12">
                    <?= $form->field($model, 'result')->widget(CodemirrorWidget::className(), [
                        'type'     => CodemirrorWidget::TYPE_CODE_MARKDOWN,
                        'options'  => ['rows' => 20],
                        'settings' => ['lineWrapping' => true],
                    ]) ?>
                </div>
            </div>

            <hr/>

            <div class="form-group">

                <div class="btn-group" role="group" aria-label="...">
                    <?= Html::button(Yii::t('tasks', 'Good result'), [
                        'class' => 'btn btn-success btn-send-result btn-lg'
                    ]) ?>
                    <?= Html::button(Yii::t('tasks', 'Fail result'), [
                        'class' => 'btn btn-danger btn-send-result btn-lg'
                    ]) ?>
                </div>

            </div>

        </div>
    </div>

    <?php ActiveForm::end(); ?>


    <div class="row">
        <div class="col-md-12">
            <?php if ($task = $model->task) : ?>
                <div class="well">
                    <div class="row">
                        <div class="col-md-4">
                            <strong><?= $task->getAttributeLabel('strategy_id') ?>: </strong>
                            <?= $task->strategy->name ?>
                        </div>
                        <div class="col-md-4">
                            <strong><?= $task->getAttributeLabel('project_id') ?>: </strong>
                            <?= $task->project
                                ? $task->project->name
                                : Html::tag('span', Yii::t('site', 'empty'), ['class' => 'text-muted']) ?>
                        </div>
                        <div class="col-md-4">
                            <strong><?= $task->getAttributeLabel('parent_id') ?>: </strong>
                            <?= $task->parent
                                ? $task->parent->name
                                : Html::tag('span', Yii::t('site', 'empty'), ['class' => 'text-muted']) ?>
                        </div>
                    </div>
                </div>

                <?php if ($task->description) : ?>
                    <h3 class=""><?= $task->getAttributeLabel('description') ?></h3>
                    <div class="well well-sm"><?= Markdown::process($task->description) ?></div>
                <?php endif; ?>


                <div class="row">
                    <div class="col-md-6">

                        <?= DetailView::widget([
                            'model'      => $task,
                            'attributes' => [
                                ['attribute' => 'is_argue', 'value' => $task->is_argue],
                                [
                                    'label' => Yii::t('tasks', 'Task result actual'),
                                    'value' => $task->resultActual
                                ],
                                [
                                    'label' => Yii::t('tasks', 'Task result expect'),
                                    'value' => $task->resultExpect
                                ],
                                [
                                    'label' => Yii::t('tasks', 'Task result accumulate'),
                                    'value' => $task->resultAccumulate
                                ],
                                [
                                    'label' => Yii::t('tasks', 'Task result percent'),
                                    'value' => $task->resultPercent
                                ],
                                'started_at:datetime',
                                'finished_at:datetime',
                            ]
                        ]) ?>
                    </div>
                    <div class="col-md-6">

                        <?= DetailView::widget([
                            'model'      => $task,
                            'attributes' => [
                                [
                                    'attribute' => 'progress',
                                    'value'     => $task->progress,
                                    'format'    => ['raw', []],
                                ],
                                ['attribute' => 'status', 'value' => $task::$statuses[$task->status]],
                                ['attribute' => 'priority', 'value' => $task::$priorities[$task->priority]],
                                ['attribute' => 'type', 'value' => $task::$types[$task->type]],
                                [
                                    'attribute' => 'price',
                                    'value'     => $task->price ? $task->price . 'р.' : null,
                                    'format'    => ['raw', []],
                                ],
                                'time:datetime',
                                'started_at:datetime',
                                'finished_at:datetime',
                                'created_at:datetime',
                            ],
                        ]) ?>

                        <?php if ($task->result) : ?>
                            <h3 class=""><?= $task->getAttributeLabel('result') ?></h3>
                            <div class="well well-sm"><?= Markdown::process($task->result) ?></div>
                        <?php endif; ?>

                        <hr/>

                        <?php $checklist = json_decode($task->checklist, true); ?>
                        <?php if (count($checklist)) : ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><?= $task->getAttributeLabel('checklist') ?></h3>
                                </div>
                                <div class="panel-body">
                                    <?php foreach ($checklist as $job) : ?>
                                        <div class="<?= $job['enabled'] ? 'text-default' : 'text-primary' ?>">
                                            <?= $job['name'] ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>

                            <hr/>
                        <?php endif; ?>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><?= Yii::t('media', 'Files') ?></h3>
                            </div>
                            <div class="panel-body">
                                <?= FileInputWidget::widget([
                                    'model'         => $task,
                                    'type'          => FileInputWidget::TYPE_MULTI,
                                    'title'         => true,
                                    'description'   => true,
                                    'showTitle'     => true,
                                    'previewHeight' => 20,
                                    'onlyView'      => true,
                                ]) ?>
                            </div>
                        </div>

                    </div>
                </div>

            <?php endif; ?>
        </div>
    </div>

    <?php if ($model->task) : ?>
        <?php foreach ($model->task->getDayResults($curDate) as $result) : ?>

            <div class="panel panel-<?= $result->status == TaskResult::STATUS_FAIL ? 'danger' : 'success' ?>">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <?= $result->created_at ?>
                        </div>
                        <div class="col-md-6 text-right">
                            <?= $result->accumulate ?>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <?= Markdown::process($result->result) ?>
                </div>
            </div>

        <?php endforeach; ?>
    <?php endif; ?>


    <?php Pjax::end(); ?>

</div>
