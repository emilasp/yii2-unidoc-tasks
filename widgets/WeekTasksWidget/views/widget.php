<?php
use emilasp\tasks\widgets\WeekTasksWidget\WeekTasksWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$countDays = count($tasksDays);
?>

<div class="row">
    <div class="col-md-2">
        <div class="tasks-all-opened">
            <?php foreach ($openTasks as $task) : ?>
                <div>
                    <?= Html::a($task->name, Url::toRoute(['/tasks/task/update', 'id' => $task->id]), [
                        'class' => 'btn btn-default btn-xs'
                    ]) ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="col-md-10">

        <div class="week-tasks-week-container">

            <?php Pjax::begin(['id' => 'tasks-week-table']); ?>

            <div class="row tasks-actions">
                <div class="col-md-6 text-left">

                    <div class="btn-toolbar" role="toolbar" aria-label="">
                        <div class="btn-group" role="group">
                            <?= Html::a(
                                Html::tag('i', '', ['class' => 'fa fa-arrow-left']),
                                Url::current(['date' => $prevDate]),
                                ['class' => 'btn btn-info']
                            ) ?>
                        </div>

                        <div class="btn-group btn-formatGrid" role="group">

                            <?= Html::button(Yii::t('tasks', '1 day'), [
                                'class'       => 'btn btn-' . ($formatGrid === WeekTasksWidget::FORMAT_ONE_DAY ? 'primary' : 'default'),
                                'data-format' => WeekTasksWidget::FORMAT_ONE_DAY
                            ]) ?>
                            <?= Html::button(Yii::t('tasks', '3 days'), [
                                'class'       => 'btn btn-' . ($formatGrid === WeekTasksWidget::FORMAT_THREE_DAYS ? 'primary' : 'default'),
                                'data-format' => WeekTasksWidget::FORMAT_THREE_DAYS
                            ]) ?>
                            <?= Html::button(Yii::t('tasks', 'week'), [
                                'class'       => 'btn btn-' . ($formatGrid === WeekTasksWidget::FORMAT_WEEK ? 'primary' : 'default'),
                                'data-format' => WeekTasksWidget::FORMAT_WEEK
                            ]) ?>

                        </div>

                        <div class="btn-group" role="group">
                            <?= Html::a(
                                Html::tag('i', '', ['class' => 'fa fa-arrow-right']),
                                Url::current(['date' => $nextDate]),
                                ['class' => 'btn btn-info']
                            ) ?>
                        </div>
                    </div>

                </div>
                <div class="col-md-6 text-right">


                </div>
            </div>


            <table class="week-tasks-table table-hover">
                <tbody>
                <tr>
                    <?php foreach ($tasksDays as $date => $times): ?>
                        <?php $weekend = Yii::$app->formatter->asDate($date, 'php:N') ?>
                        <th class="week-tasks-th<?= $weekend > 5 ? '-weekend' : '' ?> <?= date('Y-m-d') == $date ? ' current-day' : '' ?>">
                            <?= Yii::$app->formatter->asDate($date, 'php:D') ?>
                            <small><?= Yii::$app->formatter->asDate($date, 'php:d.m.Y') ?></small>
                        </th>
                    <?php endforeach; ?>
                </tr>
                <tr>
                    <?php foreach ($tasksDays as $date => $times): ?>
                        <td class="week-tasks-td <?= date('Y-m-d') == $date ? ' current-day' : '' ?>">
                            <?php foreach ($times as $time => $tasks) : ?>

                                <?php foreach ($tasks as $index => $task) : ?>

                                    <?php if (!$task->getDayCompleted($date)) : ?>
                                        <?php
                                        unset($tasksDays[$date][$time][$index]);

                                        $btnClass = WeekTasksWidget::getTaskPriorityClass($task);

                                        if (!$task->getDayCompleted($date) && $date < date('Y-m-d')) {
                                            $btnClass .= ' btn-task-failed';
                                        }
                                        ?>

                                        <?= Html::button(WeekTasksWidget::getTaskFullName($task, $date), [
                                            'class' => 'week-tasks-task btn' . $btnClass,
                                            'data'  => ['id' => $task->id, 'date' => $date],
                                        ]) ?>


                                    <?php endif; ?>

                                <?php endforeach; ?>

                            <?php endforeach; ?>
                        </td>
                    <?php endforeach; ?>
                </tr>

                <tr>
                    <td colspan="<?= $countDays ?>">Завершенные</td>
                </tr>

                <tr>
                    <?php foreach ($tasksDays as $date => $times): ?>
                        <td class="week-tasks-td <?= date('Y-m-d') == $date ? ' current-day' : '' ?>">
                            <?php foreach ($times as $time => $tasks) : ?>

                                <?php foreach ($tasks as $index => $task) : ?>

                                    <?= Html::button(
                                        WeekTasksWidget::getTaskFullName($task, $date),
                                        [
                                            'class' => 'week-tasks-task btn btn-xs btn-success task-completed',
                                            'data'  => ['id' => $task->id, 'date' => $date],
                                        ]
                                    ) ?>

                                <?php endforeach; ?>

                            <?php endforeach; ?>
                        </td>
                    <?php endforeach; ?>
                </tr>

                <tr>
                    <td colspan="<?= $countDays ?>">
                        <h3 class="text-primary">
                            <i class="fa fa-heart"></i>
                            В работе
                        </h3>

                        <?php foreach ($jobTasks as $index => $task) : ?>
                            <?= Html::a($task->name, Url::toRoute(['/tasks/task/update', 'id' => $task->id]), [
                                    'class' => 'btn btn-xs' . WeekTasksWidget::getTaskPriorityClass($task),
                                ]
                            ) ?>
                        <?php endforeach; ?>

                    </td>
                </tr>

                <tr>
                    <td colspan="<?= $countDays ?>">

                        <div class="row">
                            <div class="col-md-4">
                                <h3 class="text-danger">
                                    <i class="fa fa-exclamation-triangle"></i>
                                    <?= Yii::t('tasks', 'Highest Tasks') ?>
                                </h3>

                                <?php foreach ($highestTasks as $index => $task) : ?>
                                    <?= Html::a($task->name, Url::toRoute(['/tasks/task/update', 'id' => $task->id]), [
                                            'class' => 'btn btn-xs btn-danger',
                                        ]
                                    ) ?>
                                <?php endforeach; ?>

                            </div>
                            <div class="col-md-4">
                                <h3 class="text-info">
                                    <i class="fa fa-shopping-basket"></i>
                                    <?= Yii::t('tasks', 'Bayed Tasks') ?>
                                </h3>

                                <?php $sum = 0; ?>
                                <?php foreach ($bayTasks as $index => $task) : ?>
                                    <?php $sum += $task->price; ?>
                                    <?= Html::a(
                                        $task->name . ' - '
                                        . Html::tag('span', (int)$task->price . 'р', ['class' => 'label label-info']),
                                        Url::toRoute(['/tasks/task/update', 'id' => $task->id]),
                                        ['class' => 'btn btn-xs btn-primary']
                                    ) ?>
                                <?php endforeach; ?>

                                <div class="well well-sm">
                                    <div class="row">
                                        <div class="col-md-6">
                                            Общая сумма:
                                        </div>
                                        <div class="col-md-6 text-success text-right">
                                            <strong><?= $sum ?></strong> р.
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-4">
                                <h3 class="text-default">
                                    <i class="fa fa-cubes"></i>
                                    <?= Yii::t('tasks', 'Frog Tasks') ?>
                                </h3>

                                <?php foreach ($frogTasks as $index => $task) : ?>
                                    <?= Html::a($task->name, Url::toRoute(['/tasks/task/update', 'id' => $task->id]), [
                                            'class' => 'btn btn-xs btn-default',
                                        ]
                                    ) ?>
                                <?php endforeach; ?>

                            </div>

                        </div>
                    </td>
                </tr>

                </tbody>
            </table>

            <?php Pjax::end(); ?>
        </div>

        <?= $this->render('_form', ['curDate' => $curDate, 'model' => $model]) ?>

    </div>
</div>
