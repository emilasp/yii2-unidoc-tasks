<?php
namespace emilasp\tasks\widgets\PeriodicalInput;

use yii\helpers\Html;
use yii\widgets\InputWidget;

/**
 * Виджет формирует выбор дней недели, дней месяца и кастомных дат
 *
 * Class PeriodicalInput
 * @package emilasp\tasks\widgets\PeriodicalInput
 */
class PeriodicalInput extends InputWidget
{
    const TYPE_WEEK_DAYS    = 'week-days';
    const TYPE_MONTH_DAYS   = 'month-days';
    const TYPE_CUSTOM_DATES = 'custom-dates';

    public $type = self::TYPE_MONTH_DAYS;

    public $renderOptions = [
        self::TYPE_MONTH_DAYS   => [
            1  => 1,
            2  => 2,
            3  => 3,
            4  => 4,
            5  => 5,
            6  => 6,
            7  => 7,
            8  => 8,
            9  => 9,
            10 => 10,
            11 => 11,
            12 => 12,
            13 => 13,
            14 => 14,
            15 => 15,
            16 => 16,
            17 => 17,
            18 => 18,
            19 => 19,
            20 => 20,
            21 => 21,
            22 => 22,
            23 => 23,
            24 => 24,
            25 => 25,
            26 => 26,
            27 => 27,
            28 => 28,
            29 => 29,
            30 => 30,
            31 => 31
        ],
        self::TYPE_WEEK_DAYS    => [1 => 'Пн', 2 => 'Вт', 3 => 'Ср', 4 => 'Чт', 5 => 'Пт', 6 => 'Сб', 7 => 'Вс'],
        self::TYPE_CUSTOM_DATES => [],
    ];

    public function init()
    {
        parent::init();

        $this->registerAssets();
    }

    public function run()
    {
        echo Html::beginTag('div', ['class' => 'periodical-container']);

        if ($this->type === self::TYPE_CUSTOM_DATES) {
            echo Html::beginTag('div', ['class' => 'row']);
            echo Html::beginTag('div', ['class' => 'col-md-2']);
            echo $this->render($this->type, ['options' => $this->getOptions()]);
            echo Html::endTag('div');
            echo Html::beginTag('div', ['class' => 'col-md-10']);
            echo Html::activeTextInput($this->model, $this->attribute, ['class' => 'form-control periodical-input']);
            echo Html::endTag('div');
            echo Html::endTag('div');
        } else {
            echo Html::activeHiddenInput($this->model, $this->attribute, ['class' => 'periodical-input']);
            echo $this->render($this->type, ['options' => $this->getOptions()]);
        }


        echo Html::endTag('div');
    }

    /**
     * Registers the needed assets
     */
    protected function registerAssets()
    {
        PeriodicalInputAsset::register($this->view);
    }

    /**
     * Формируем опции дял рендеринга
     *
     * @return array
     */
    private function getOptions()
    {
        $renderOptions = $this->renderOptions[$this->type];
        $values        = (array)json_decode($this->model->{$this->attribute}, true);
        $options       = [];
        foreach ($renderOptions as $value => $name) {
            $class = 'text-muted';
            if ($this->type === self::TYPE_WEEK_DAYS && $value >= 6) {
                $class .= ' weekend';
            }

            $options[$value] = [
                'name'  => $name,
                'class' => in_array($value, $values) ? $class . ' selected' : $class
            ];
        }
        return $options;
    }
}
