<?php
use yii\jui\DatePicker;
use yii\web\JsExpression;

$js = <<<JS
    function(dateText, inst) {
        var input = $('#task-customdates');
        var json = input.val();
        if (!json) {
            json = '[]';
        }
        var dates = JSON.parse(json);
        dates.push(dateText);
        input.val(JSON.stringify(dates));;
    }
JS;
?>

<?= DatePicker::widget([
    'name'          => 'custom_date_select',
    'dateFormat'    => 'php:Y-m-d',
    'options'       => [
        'class'       => 'form-control',
        'placeholder' => Yii::t('site', 'Select date'),
    ],
    'clientOptions' => [
        'onSelect' => new JsExpression($js),
    ],
]) ?>