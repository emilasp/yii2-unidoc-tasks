<table border="0" cellpadding="1" cellspacing="1" class="periodical-calendar">
    <tbody>
    <tr>
        <?php foreach ($options as $value => $option) : ?>
            <td data-value="<?= $value ?>" class="<?= $option['class'] ?>"><?= $option['name'] ?></td>
        <?php endforeach; ?>
    </tr>
    </tbody>
</table>