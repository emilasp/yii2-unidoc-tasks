$('body').on('click', '.periodical-calendar td', function (event) {
    var td = $(this);
    var container = td.closest('.periodical-container');

    if (td.hasClass('selected')) {
        td.removeClass('selected');
    } else {
        td.addClass('selected');
    }

    var data = [];

    container.find('td').each(function (index, element) {
        var el = $(element);
        if (el.hasClass('selected')) {
            data.push(el.data('value').toString());
        }
    });
    container.find('.periodical-input').val(JSON.stringify(data));
});