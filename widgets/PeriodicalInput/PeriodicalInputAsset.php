<?php
namespace emilasp\tasks\widgets\PeriodicalInput;

use yii\web\AssetBundle;

/**
 * Class PeriodicalInputAsset
 * @package emilasp\tasks\widgets\PeriodicalInput
 */
class PeriodicalInputAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $js  = ['periodical-input.js'];
    public $css = ['periodical-input.css'];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];
}
