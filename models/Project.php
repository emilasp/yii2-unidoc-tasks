<?php

namespace emilasp\tasks\models;

use emilasp\media\behaviors\FileBehavior;
use emilasp\taxonomy\behaviors\TagBehavior;
use emilasp\users\common\models\User;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "unidoc_tasks_project".
 *
 * @property integer  $id
 * @property integer  $strategy_id
 * @property string   $name
 * @property string   $description
 * @property integer  $progress
 * @property integer  $type
 * @property integer  $status
 * @property string   $created_at
 * @property string   $updated_at
 * @property integer  $created_by
 * @property integer  $updated_by
 *
 * @property Strategy $strategy
 * @property User     $createdBy
 * @property User     $updatedBy
 * @property Task[]   $tasks
 */
class Project extends \emilasp\unidoc\models\Project
{
    public static $taskType = Task::TASK_TYPE_TASK;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([], parent::behaviors());
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['strategy_id', 'name', 'status'], 'required'],
            [['strategy_id', 'progress', 'type', 'status', 'created_by', 'updated_by'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [
                ['strategy_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Strategy::className(),
                'targetAttribute' => ['strategy_id' => 'id']
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id']
            ],
            ['formTags', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('tasks', 'ID'),
            'strategy_id' => Yii::t('tasks', 'Стратегия'),
            'name'        => Yii::t('tasks', 'Наименование'),
            'description' => Yii::t('tasks', 'Описание'),
            'progress'    => Yii::t('tasks', 'Прогресс'),
            'status'      => Yii::t('tasks', 'Статус'),
            'type'        => Yii::t('tasks', 'Type'),
            'created_at'  => Yii::t('tasks', 'Создан'),
            'updated_at'  => Yii::t('tasks', 'Изменен'),
            'created_by'  => Yii::t('tasks', 'Автор'),
            'updated_by'  => Yii::t('tasks', 'Изменил'),
        ];
    }
}
