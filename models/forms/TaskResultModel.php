<?php

namespace emilasp\tasks\models\forms;

use emilasp\tasks\models\Task;
use emilasp\unidoc\models\TaskResult;
use Yii;
use yii\base\Model;

/**
 * Class TaskResultModel
 * @package emilasp\tasks\models\forms
 */
class TaskResultModel extends Model
{
    public $task_id;
    public $is_argue;
    public $status     = TaskResult::STATUS_NEW;
    public $result;
    public $accumulate = 1;

    public $date;
    public $task;
    public $results = [];

    public function rules()
    {
        return [
            [['task_id', 'status'], 'required'],
            [['task_id', 'is_argue', 'status', 'accumulate'], 'integer'],
            [
                'result',
                'required',
                'when' => function ($model) {
                    return $model->is_argue;
                }
            ],
            ['result', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'task_id'    => 'Задача',
            'is_argue'   => 'Аргументировать',
            'accumulate' => 'Наработано',
            'result'     => 'Результат',
            'status'     => 'Статус',
        ];
    }

    public function init()
    {
        parent::init();

        $this->fillModel();
    }

    public function save()
    {
        $this->fillModel();

        $result = new TaskResult();
        $result->setAttributes($this->getAttributes());

        if (!$result->save()) {
            foreach ($result->getErrors() as $error) {
                $this->addError('result', $error[0]);
            }
        }
    }

    /**
     * Заполняем модель
     */
    public function fillModel(): void
    {
        if ($this->task_id) {
            $this->task     = Task::findOne($this->task_id);
            $this->is_argue = $this->task->is_argue;
            $this->results  = TaskResult::find()->byCreatedBy()->where([
                'task_id'          => $this->task_id,
                'DATE(created_at)' => date('Y-m-d', strtotime($this->date))
            ])->orderBy('created_at DESC')->all();
        }
    }
}