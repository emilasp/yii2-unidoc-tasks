<?php
namespace emilasp\tasks\models;

use emilasp\json\behaviors\JsonFieldBehavior;
use emilasp\tasks\behaviors\PeriodicalBehavior;
use emilasp\tasks\behaviors\TaskGoallBehavior;
use emilasp\users\common\models\User;
use emilasp\variety\behaviors\VarietyModelBehavior;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

/**
 * This is the model class for table "unidoc_tasks_task".
 *
 * @property integer  $id
 * @property integer  $strategy_id
 * @property integer  $project_id
 * @property integer  $parent_id
 * @property string   $name
 * @property string   $description
 * @property string   $result
 * @property string   $checklist
 * @property integer  $progress
 * @property integer  $priority
 * @property integer  $task_type
 * @property integer  $make_type
 * @property integer  $type
 * @property string   $price
 * @property integer  $time
 * @property string   $metric
 * @property integer  $is_argue
 * @property integer  $status
 * @property string   $started_at
 * @property string   $finished_at
 * @property string   $created_at
 * @property string   $updated_at
 * @property integer  $created_by
 * @property integer  $updated_by
 *
 * @property Strategy $strategy
 * @property Project  $project
 * @property Task     $parent
 * @property Task[]   $tasks
 * @property User     $createdBy
 * @property User     $updatedBy
 */
class Task extends \emilasp\unidoc\models\Task
{
    public static $taskType = Task::TASK_TYPE_TASK;

    /** @var integer Metrics */
    public $metricResultType;
    public $metricResultMicro;
    public $metricResultMacro;
    public $metricResultActual;
    public $metricResultExpect;
    public $metricResultAccumulate;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class'     => JsonFieldBehavior::className(),
                'attribute' => 'checklist',
                'scheme'    => [
                    'enabled'     => ['label' => 'Enabled', 'rules' => []],
                    'name'        => ['label' => 'Name', 'rules' => [['string']]],
                    'description' => ['label' => 'Description', 'rules' => [['string']]],
                ],
            ],
            'periodicals' => [
                'class' => PeriodicalBehavior::className(),
            ],
            'goals'       => [
                'class' => TaskGoallBehavior::className(),
            ],
            'variety_on_index' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'is_argue',
                'group'     => 'bool',
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['task_type', 'default', 'value' => static::$taskType],
            [['strategy_id', 'name', 'status'], 'required'],
            [
                 'result',
                 'required',
                 'when' => function ($model) {
                     return $model->is_argue && in_array($model->status, [self::STATUS_FINISH, self::STATUS_CANCEL]);
                 },
                 'whenClient' => new JsExpression('function(attribute, value) {
                    var is_argue = attribute.$form.find("#task-is_argue").val();
                    var status   = attribute.$form.find("#task-status").val();
                    
                    console.log(is_argue, status);
                    
                    if (is_argue == 1 && (status == 2 || status == 4)) {
                        console.log(1);
                        return true;
                    }
                    return false;
                  }')
             ],

            [
                [
                    'strategy_id',
                    'project_id',
                    'parent_id',
                    'progress',
                    'priority',
                    'type',
                    'make_type',
                    'is_argue',
                    'status',
                    'created_by',
                    'updated_by'
                ],
                'integer'
            ],
            [['description', 'result', 'checklist'], 'string'],

            [['price'], 'number'],
            [['started_at', 'finished_at', 'created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [
                ['project_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Project::className(),
                'targetAttribute' => ['project_id' => 'id']
            ],
            [
                ['strategy_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Strategy::className(),
                'targetAttribute' => ['strategy_id' => 'id']
            ],
            [
                ['parent_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Task::className(),
                'targetAttribute' => ['parent_id' => 'id']
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id']
            ],
            ['formTags', 'safe'],
            [['monthDays', 'weekDays', 'customDates', 'time', 'metric', 'metricResultActual'], 'safe'],

            [
                [
                    'metricResultType',
                    'metricResultMicro',
                    'metricResultMacro',
                    'metricResultExpect',
                    'metricResultAccumulate'
                ],
                'integer'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge([
            'metricResultMicro'  => 'Должно быть выполнено за день',
            'metricResultMacro'  => 'Должно быть выполнено всего',
            'metricResultActual' => 'Результат на текущий момент',
            'metricResultExpect' => 'Ожидаемый результат(осталось)',
        ], parent::attributeLabels());
    }

    /**
     * Получаем файлы привязанные к моделе
     *
     * @return Query
     */
    public function getPeriod()
    {
        return $this->hasOne(TaskPeriod::className(), ['object_id' => 'id'])->where(['object' => self::className()]);
    }
}
