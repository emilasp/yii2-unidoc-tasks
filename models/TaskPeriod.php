<?php

namespace emilasp\tasks\models;

use emilasp\core\components\base\ActiveRecord;
use emilasp\users\common\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\BaseActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "unidoc_tasks_period".
 *
 * @property integer $id
 * @property string  $month_days
 * @property string  $week_days
 * @property string  $custom_dates
 * @property string  $object
 * @property integer $object_id
 * @property string  $created_at
 * @property integer $created_by
 *
 * @property User    $createdBy
 * @property User    $updatedBy
 */
class TaskPeriod extends ActiveRecord
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [BaseActiveRecord::EVENT_BEFORE_INSERT => ['created_at']],
                'value'      => new Expression('NOW()'),
            ],
            [
                'class'      => BlameableBehavior::className(),
                'attributes' => [BaseActiveRecord::EVENT_BEFORE_INSERT => ['created_by']]
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unidoc_tasks_period';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['month_days', 'week_days', 'custom_dates'], 'string'],
            [['object', 'object_id'], 'required'],
            [['object_id', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
            [['object'], 'string', 'max' => 128],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => Yii::t('tasks', 'ID'),
            'month_days'   => Yii::t('tasks', 'Month Days'),
            'week_days'    => Yii::t('tasks', 'Week Days'),
            'custom_dates' => Yii::t('tasks', 'Custom Dates'),
            'object'       => Yii::t('tasks', 'Object'),
            'object_id'    => Yii::t('tasks', 'Object ID'),
            'created_at'   => Yii::t('tasks', 'Created At'),
            'created_by'   => Yii::t('tasks', 'Created By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
}
