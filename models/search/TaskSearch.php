<?php

namespace emilasp\tasks\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use emilasp\tasks\models\Task;

/**
 * TaskSearch represents the model behind the search form of `emilasp\tasks\models\Task`.
 */
class TaskSearch extends Task
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'strategy_id',
                    'project_id',
                    'parent_id',
                    'progress',
                    'priority',
                    'type',
                    'make_type',
                    'time',
                    'is_argue',
                    'status',
                    'created_by',
                    'updated_by'
                ],
                'integer'
            ],
            [
                [
                    'name',
                    'description',
                    'result',
                    'checklists',
                    'metric',
                    'started_at',
                    'finished_at',
                    'created_at',
                    'updated_at'
                ],
                'safe'
            ],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Task::find();

        $dataProvider = new ActiveDataProvider(['query' => $query]);
        $dataProvider->setSort(['defaultOrder' => ['id'=>SORT_DESC]]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'task_type'   => static::$taskType,
            'id'          => $this->id,
            'strategy_id' => $this->strategy_id,
            'project_id'  => $this->project_id,
            'parent_id'   => $this->parent_id,
            'progress'    => $this->progress,
            'priority'    => $this->priority,
            'type'        => $this->type,
            'make_type'        => $this->type,
            'price'       => $this->price,
            'time'        => $this->time,
            'is_argue'    => $this->is_argue,
            'status'      => $this->status,
            'started_at'  => $this->started_at,
            'finished_at' => $this->finished_at,
            'created_at'  => $this->created_at,
            'updated_at'  => $this->updated_at,
            'created_by'  => $this->created_by,
            'updated_by'  => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'result', $this->result])
            ->andFilterWhere(['like', 'metric', $this->metric])
            ->andFilterWhere(['like', 'checklists', $this->checklists]);

        return $dataProvider;
    }
}
