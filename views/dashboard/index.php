<?php

use emilasp\tasks\models\Project;
use emilasp\tasks\models\Strategy;
use emilasp\tasks\widgets\WeekTasksWidget\WeekTasksWidget;
use kartik\grid\DataColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\tasks\models\search\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Html::tag('span', '', ['class' => 'fa fa-list text-primary']) . ' ' . Yii::t('tasks', 'Dashboard');

$this->params['breadcrumbs'][] = Yii::t('tasks', 'Projects');
?>
<div class="dashboard-index">

    <?= WeekTasksWidget::widget() ?>

</div>
