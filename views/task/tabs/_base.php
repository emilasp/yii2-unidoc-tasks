<?php
use dosamigos\selectize\SelectizeTextInput;
use emilasp\core\extensions\CodemirrorWidget\CodemirrorWidget;
use emilasp\json\models\DynamicModel;
use emilasp\json\widgets\DynamicFields\DynamicFields;


/* @var $this yii\web\View */
/* @var $model emilasp\tasks\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="base" class="tab-pane fade in active clearfix">

    <div class="row">
        <div class="col-md-6">

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'description')->widget(CodemirrorWidget::className(), [
                'type'     => CodemirrorWidget::TYPE_CODE_MARKDOWN,
                'options'  => ['rows' => 20],
                'settings' => ['lineWrapping' => true],
            ]) ?>


        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'type', [
                        'addon' => [
                            'groupOptions' => ['class' => 'input-group-sm'],
                            'prepend'      => ['content' => '<i class="fa fa-lock"></i>']
                        ]
                    ])->dropDownList($model::$types) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'priority', [
                        'addon' => [
                            'groupOptions' => ['class' => 'input-group-sm'],
                            'prepend'      => ['content' => '<i class="fa fa-lock"></i>']
                        ]
                    ])->dropDownList($model::$priorities) ?>
                </div>
            </div>



            <?= $form->field($model, 'progress')->textInput() ?>

            <?= $form->field($model, 'formTags')->widget(SelectizeTextInput::className(), [
                'loadUrl'       => ['/taxonomy/tag/search'],
                'options'       => ['class' => 'form-control'],
                'clientOptions' => [
                    'plugins'     => ['remove_button', 'restore_on_backspace'],
                    'valueField'  => 'name',
                    'labelField'  => 'name',
                    'searchField' => ['name'],
                    'create'      => true,
                ],
            ])->hint('Используйте запятые для разделения меток') ?>


            <?= DynamicFields::widget([
                'form'        => $form,
                'model'       => $model,
                'attribute'   => 'checklist',
                'addEmptyRow' => true,
                'scheme'      => DynamicModel::SCHEME_CUSTOM,
                'viewFile'    => '@vendor/emilasp/yii2-unidoc-tasks/views/task/json/_checklist',
            ]); ?>

        </div>
    </div>
</div>