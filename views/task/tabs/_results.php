<?php
use emilasp\core\extensions\CodemirrorWidget\CodemirrorWidget;
use yii\helpers\Markdown;

/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\Good */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="results" class="tab-pane fade clearfix">

    <h2><?= Yii::t('tasks', 'Results') ?></h2>

    <?= $form->field($model, 'is_argue')->dropDownList($model->is_argues) ?>

    <?= $form->field($model, 'result')->widget(CodemirrorWidget::className(), [
        'type'     => CodemirrorWidget::TYPE_CODE_MARKDOWN,
        'options'  => ['rows' => 20],
        'settings' => ['lineWrapping' => true],
    ]) ?>

    <h3><?= Yii::t('tasks', 'Results') ?></h3>

    <?php foreach ($model->results as $result) : ?>
        <div class="row">
            <div class="col-md-6"><?= $result::$statuses[$result->status] ?></div>
            <div class="col-md-6 text-right"><?= $result->created_at ?></div>
        </div>
        <div><?= Markdown::process($result->result) ?></div>
        <hr />
    <?php endforeach; ?>

</div>
