<?php
use emilasp\media\extensions\FileInputWidget\FileInputWidget;

/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\Good */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="files" class="tab-pane fade clearfix">

    <h2><?= Yii::t('media', 'Files') ?></h2>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Yii::t('media', 'Files') ?></h3>
        </div>
        <div class="panel-body">
            <?= FileInputWidget::widget([
                'model'         => $model,
                'type'          => FileInputWidget::TYPE_MULTI,
                'title'         => true,
                'description'   => true,
                'showTitle'     => true,
                'previewHeight' => 20,
            ]) ?>
        </div>
    </div>

</div>
