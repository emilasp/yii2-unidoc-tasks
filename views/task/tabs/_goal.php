<?php

/* @var $this yii\web\View */
use emilasp\tasks\behaviors\TaskGoallBehavior;

/* @var $model emilasp\im\common\models\Good */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="goal" class="tab-pane fade clearfix">

    <h2><?= Yii::t('tasks', 'Goal') ?></h2>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'metricResultType')->dropDownList(TaskGoallBehavior::$resultTypes) ?>

            <?= $form->field($model, 'metricResultMacro')->input('number', ['maxlength' => true]) ?>
            <?= $form->field($model, 'metricResultMicro')->input('number', ['maxlength' => true]) ?>


            <?= $form->field($model, 'metricResultAccumulate')->input('number', ['maxlength' => true]) ?>
           <!-- <?/*= $form->field($model, 'metricResultActual')->input('number', ['maxlength' => true]) */?>
            --><?/*= $form->field($model, 'metricResultExpect')->input('number', ['maxlength' => true]) */?>
        </div>
        <div class="col-md-6">

        </div>
    </div>

</div>
