<?php
/* @var $this yii\web\View */
use emilasp\geoapp\widgets\AddressWidget\AddressWidget;
use emilasp\json\models\DynamicModel;
use emilasp\json\widgets\DynamicFields\DynamicFields;
use emilasp\tasks\widgets\PeriodicalInput\PeriodicalInput;
use kartik\time\TimePicker;
use yii\jui\DatePicker;
use yii\widgets\MaskedInput;

?>
<div id="dates" class="tab-pane fade clearfix">

    <h2><?= Yii::t('tasks', '') ?></h2>

    <div class="row">
        <div class="col-md-6">

            <div class="panel panel-default">
                <div class="panel-heading"><?= Yii::t('site', 'Base') ?></div>
                <div class="panel-body">
                    <?= $form->field($model, 'make_type', [
                        'addon' => [
                            'groupOptions' => ['class' => 'input-group-sm'],
                            'prepend'      => ['content' => '<i class="fa fa-lock"></i>']
                        ]
                    ])->dropDownList($model::$make_types) ?>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'started_at')->widget(DatePicker::classname(), [
                                'language'   => 'ru',
                                'dateFormat' => 'yyyy-MM-dd',
                                'options'    => ['class' => 'form-control']
                            ]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'finished_at')->widget(DatePicker::classname(), [
                                'language'   => 'ru',
                                'dateFormat' => 'yyyy-MM-dd',
                                'options'    => ['class' => 'form-control']
                            ]) ?>
                        </div>
                    </div>

                    <?= $form->field($model, 'time')->widget(TimePicker::classname(), [
                        'pluginOptions' => [
                            'showSeconds'  => true,
                            'showMeridian' => false,
                            'minuteStep'   => 1,
                            'secondStep'   => 5,
                        ]
                    ]) ?>
                </div>
            </div>

        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><?= Yii::t('tasks', 'Periodicals') ?></div>
                <div class="panel-body">
                    <?= $form->field($model, 'monthDays')->widget(PeriodicalInput::className(), [
                        'type' => PeriodicalInput::TYPE_MONTH_DAYS
                    ]) ?>

                    <?= $form->field($model, 'weekDays')->widget(PeriodicalInput::className(), [
                        'type' => PeriodicalInput::TYPE_WEEK_DAYS
                    ]) ?>

                    <?= $form->field($model, 'customDates')->widget(PeriodicalInput::className(), [
                        'type' => PeriodicalInput::TYPE_CUSTOM_DATES
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

</div>
