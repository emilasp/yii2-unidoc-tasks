<?php
use emilasp\tasks\models\Project;
use emilasp\tasks\models\Strategy;
use emilasp\tasks\models\Task;
use kartik\form\ActiveForm;
use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model emilasp\tasks\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-form">

    <?php $form = ActiveForm::begin([
        'id'          => 'learn-form',
        'fieldConfig' => ['autoPlaceholder' => false],
        'formConfig'  => ['deviceSize' => 'sm']
    ]); ?>

    <?= $form->errorSummary($model, ['header' => '']); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'strategy_id')->dropDownList(
                Strategy::find()->map()->byCreatedBy()->all(),
                ['id' => 'tasks-strategy_id']
            ) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'project_id')->widget(DepDrop::classname(), [
                'options'       => ['id' => 'tasks-project_id'],
                'data'          => ArrayHelper::merge(['' => '-select project-'], Project::find()
                    ->filterWhere(['strategy_id' => $model->strategy_id])
                    ->map()
                    ->byCreatedBy()
                    ->all()),
                'pluginOptions' => [
                    'depends'     => ['tasks-strategy_id'],
                    'placeholder' => Yii::t('tasks', 'Select project'),
                    'url'         => Url::to(['/tasks/project/strategy-depend'])
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'parent_id')->widget(DepDrop::classname(), [
                'data'           => ArrayHelper::merge(['' => Yii::t('site', 'select')],
                    Task::find()
                        ->filterWhere(['project_id' => $model->project_id])
                        ->map()
                        ->byCreatedBy()
                        ->all()),
                'type'           => DepDrop::TYPE_SELECT2,
                'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                'pluginOptions'  => [
                    'depends'     => ['tasks-strategy_id', 'tasks-project_id'],
                    'placeholder' => Yii::t('tasks', 'Select parent task'),
                    'url'         => Url::to(['/tasks/task/depend'])
                ]
            ]) ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'price')->input('number', ['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'status', [
                'addon' => [
                    'groupOptions' => ['class' => 'input-group-sm'],
                    'prepend'      => ['content' => '<i class="fa fa-lock"></i>']
                ]
            ])->dropDownList($model::$statuses) ?>
        </div>
    </div>

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#base"><?= Yii::t('site', 'Tab base') ?></a></li>
        <li><a data-toggle="tab" href="#dates"><?= Yii::t('tasks', 'Date and periodical properties') ?></a></li>
        <li><a data-toggle="tab" href="#goal"><?= Yii::t('tasks', 'Goal') ?></a></li>
        <li><a data-toggle="tab" href="#files"><?= Yii::t('media', 'Attachment files') ?></a></li>
        <li><a data-toggle="tab" href="#results"><?= Yii::t('tasks', 'Results') ?></a></li>
    </ul>

    <div class="tab-content">
        <?= $this->render('tabs/_base', ['form' => $form, 'model' => $model]) ?>
        <?= $this->render('tabs/_dates', ['form' => $form, 'model' => $model]) ?>
        <?= $this->render('tabs/_goal', ['form' => $form, 'model' => $model]) ?>
        <?= $this->render('tabs/_files', ['form' => $form, 'model' => $model]) ?>
        <?= $this->render('tabs/_results', ['form' => $form, 'model' => $model]) ?>
    </div>


    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
