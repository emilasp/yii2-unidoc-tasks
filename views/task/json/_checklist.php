
<div class="item panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title pull-left"><?= Yii::t('tasks', 'Checklist') ?></h3>

        <div class="pull-right">
            <button type="button" class="add-item btn btn-success btn-xs"><i
                    class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="panel-body">

        <?php foreach ($models as $i => $model) : ?>
            <div class="row row-item">
                <div class="col-sm-1">
                    <?= $form->field($model, "[{$i}]enabled")->checkbox(['label' => false])->label(false) ?>
                </div>

                <div class="col-sm-6">
                    <?= $form->field($model, "[{$i}]name")
                        ->textInput(['maxlength' => true, 'placeholder' => 'name'])->label(false) ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, "[{$i}]description")
                        ->textInput(['maxlength' => true, 'placeholder' => 'description'])->label(false) ?>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="remove-item btn btn-danger btn-xs">
                        <i class="glyphicon glyphicon-minus"></i>
                    </button>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>
