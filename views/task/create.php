<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\tasks\models\Task */

$this->title = Html::tag('span', '', ['class' => 'fa fa-plus text-success']) . ' ' . Yii::t('tasks', 'Create Task');
$this->params['breadcrumbs'][] = ['label' => Yii::t('tasks', 'Tasks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('tasks', 'Create Task');
?>
<div class="task-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
