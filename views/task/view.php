<?php

use emilasp\media\extensions\FileInputWidget\FileInputWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Markdown;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model emilasp\tasks\models\Task */

$this->title                   = Html::tag('span', '', ['class' => 'fa fa-eye text-info']) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('tasks', 'Tasks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="task-view">

    <p>
        <?= Html::a(Yii::t('site', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('site', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => Yii::t('site', 'Are you sure you want to delete this item?'),
                'method'  => 'post',
            ],
        ]) ?>
    </p>

    <div class="well">
        <div class="row">
            <div class="col-md-4">
                <strong><?= $model->getAttributeLabel('strategy_id') ?>: </strong>
                <?= $model->strategy->name ?>
            </div>
            <div class="col-md-4">
                <strong><?= $model->getAttributeLabel('project_id') ?>: </strong>
                <?= $model->project
                    ? $model->project->name : Html::tag('span', Yii::t('site', 'empty'), ['class' => 'text-muted']) ?>
            </div>
            <div class="col-md-4">
                <strong><?= $model->getAttributeLabel('parent_id') ?>: </strong>
                <?= $model->parent
                    ? $model->parent->name : Html::tag('span', Yii::t('site', 'empty'), ['class' => 'text-muted']) ?>
            </div>
        </div>
    </div>

    <?php if ($model->description) : ?>
        <h3 class=""><?= $model->getAttributeLabel('description') ?></h3>
        <div class="well well-sm"><?= Markdown::process($model->description) ?></div>
    <?php endif; ?>


    <div class="row">
        <div class="col-md-6">

            <?= DetailView::widget([
                'model'      => $model,
                'attributes' => [
                    [
                        'attribute' => 'progress',
                        'value'     => $model->progress,
                        'format'    => ['raw', []],
                    ],
                    ['attribute' => 'status', 'value' => $model::$statuses[$model->status]],
                    ['attribute' => 'priority', 'value' => $model::$priorities[$model->priority]],
                    ['attribute' => 'type', 'value' => $model::$types[$model->type]],
                    [
                        'attribute' => 'price',
                        'value'     => $model->price ? $model->price . 'р.' : null,
                        'format'    => ['raw', []],
                    ],
                    'time:datetime',
                    'started_at:datetime',
                    'finished_at:datetime',
                    'created_at:datetime',
                ],
            ]) ?>
        </div>
        <div class="col-md-6">

            <?php if ($model->result) : ?>
                <h3 class=""><?= $model->getAttributeLabel('result') ?></h3>
                <div class="well well-sm"><?= Markdown::process($model->result) ?></div>
            <?php endif; ?>

            <hr/>

            <?php $checklist = json_decode($model->checklist, true); ?>
            <?php if (count($checklist)) : ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= $model->getAttributeLabel('checklist') ?></h3>
                    </div>
                    <div class="panel-body">
                        <?php foreach ($checklist as $job) : ?>
                            <div class="<?= $job['enabled'] ? 'text-default' : 'text-primary' ?>">
                                <?= $job['name'] ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>

                <hr/>
            <?php endif; ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Yii::t('media', 'Files') ?></h3>
                </div>
                <div class="panel-body">
                    <?= FileInputWidget::widget([
                        'model'         => $model,
                        'type'          => FileInputWidget::TYPE_MULTI,
                        'title'         => true,
                        'description'   => true,
                        'showTitle'     => true,
                        'previewHeight' => 20,
                        'onlyView'      => true,
                    ]) ?>
                </div>
            </div>

        </div>
    </div>


</div>
