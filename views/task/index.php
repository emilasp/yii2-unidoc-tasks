<?php

use emilasp\tasks\models\Project;
use emilasp\tasks\models\Strategy;
use emilasp\tasks\models\Task;
use kartik\grid\DataColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\tasks\models\search\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Html::tag('span', '', ['class' => 'fa fa-list text-primary']) . ' '
    . Yii::t('tasks', 'Tasks');
$this->params['breadcrumbs'][] = Yii::t('tasks', 'Tasks');
?>
<div class="task-index">

    <p><?= Html::a(Yii::t('tasks', 'Create Task'), ['create'], ['class' => 'btn btn-success']) ?></p>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',

            [
                'attribute' => 'strategy_id',
                'class'     => DataColumn::className(),
                'value'     => function ($model, $key, $index, $column) {
                    return $model->strategy->name;
                },
                'filter'    => Strategy::find()->byCreatedBy()->map()->cache()->orderBy('name')->all(),
            ],

            [
                'attribute' => 'project_id',
                'class'     => DataColumn::className(),
                'value'     => function ($model, $key, $index, $column) {
                    return ArrayHelper::getValue($model->project, 'name', null);
                },
                'filter'    => Project::find()->byCreatedBy()->map()->cache()->orderBy('name')->all(),
            ],
            [
                'attribute' => 'parent_id',
                'class'     => DataColumn::className(),
                'value'     => function ($model, $key, $index, $column) {
                    return ArrayHelper::getValue($model->parent, 'name', null);
                },
                'filter'    => Task::find()->byCreatedBy()->map()->cache()->orderBy('name')->all(),
            ],
            [
                'attribute' => 'progress',
                'class'     => DataColumn::className(),
                'value'     => function ($model, $key, $index, $column) {
                    return \yii\jui\ProgressBar::widget(['clientOptions' => ['value' => $model->progress]]);
                },
                'format'    => 'raw',
            ],


            // 'description:ntext',
            // 'result:ntext',
            // 'checklists',

             'price',
             'time:datetime',
             'started_at',
             'finished_at',
            [
                'attribute' => 'type',
                'class'     => DataColumn::className(),
                'value'     => function ($model, $key, $index, $column) {
                    return Task::$types[$model->type];
                },
                'filter'    => Task::$types
            ],
            [
                'attribute' => 'priority',
                'class'     => DataColumn::className(),
                'value'     => function ($model, $key, $index, $column) {
                    return Task::$priorities[$model->priority];
                },
                'filter'    => Task::$priorities
            ],
            [
                'attribute' => 'status',
                'class'     => DataColumn::className(),
                'value'     => function ($model, $key, $index, $column) {
                    return Task::$statuses[$model->status];
                },
                'filter'    => Task::$statuses
            ],
            [
                'attribute' => 'created_by',
                'value'     => function ($model) {
                    return ArrayHelper::getValue($model->createdBy, 'username', null);
                },
                'class'     => DataColumn::className(),
                'width'     => '150px',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
