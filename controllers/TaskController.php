<?php

namespace emilasp\tasks\controllers;

use Yii;
use emilasp\tasks\models\Task;
use emilasp\tasks\models\search\TaskSearch;
use emilasp\core\components\base\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => [
                    'index',
                    'view',
                    'create',
                    'update',
                    'delete',
                ],
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'view',
                            'create',
                            'update',
                            'delete',
                        ],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'depend' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new TaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Task model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, Task::className()),
        ]);
    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Task();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/tasks/dashboard/index', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Task model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, Task::className());
        $model->setTags();
        $model->setPeriods();
        $model->fillMetrics();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/tasks/dashboard/index', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Task model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id, Task::className())->delete();

        return $this->redirect(['index']);
    }


    /**
     * Зависимый селект для получения проектов
     *
     * @return array
     */
    public function actionDepend()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $out = ['output' => '', 'selected' => ''];
        if ($parents = Yii::$app->request->post('depdrop_parents', [])) {
            $out['output'] = Task::find()
                ->asArray(['id', 'name'])
                ->where([
                'strategy_id' => empty($parents[0]) ? null : (int)$parents[0],
                'project_id'  => empty($parents[1]) ? null : (int)$parents[1],
            ])
                ->byCreatedBy()
                ->all();

            if ($out['output']) {
                $out['selected'] = $out['output'][0]['id'];
            } else {
                $out['output'] = '';
            }
        }
        return $out;
    }
}
