<?php
namespace emilasp\tasks;

use emilasp\unidoc\UnidocModule;

/**
 * Class TasksModule
 * @package emilasp\tasks
 */
class TasksModule extends UnidocModule
{
    public $defaultRoute = 'tasks';
    public $controllerNamespace = 'emilasp\tasks\controllers';


    public function init()
    {
        parent::init();
    }
}
