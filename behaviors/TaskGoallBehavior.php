<?php

namespace emilasp\tasks\behaviors;

use emilasp\core\components\base\BaseActiveQuery;
use emilasp\core\helpers\DateHelper;
use emilasp\tasks\models\Task;
use emilasp\unidoc\models\TaskResult;
use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Поведение добавляет функционал работы с периодами
 *
 * Class TaskGoallBehavior
 * @package emilasp\tasks\behaviors
 */
class TaskGoallBehavior extends Behavior
{
    const RESULT_TYPE_SINGLE       = 1;
    const RESULT_TYPE_ACCUMULATION = 2;

    public static $resultTypes = [
        self::RESULT_TYPE_SINGLE       => 'Обычная',
        self::RESULT_TYPE_ACCUMULATION => 'Накопление',
    ];

    private $metrics;
    private $resultsDay = [];

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'setMetrics',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'setMetrics',
        ];
    }

    /**
     * Сохраняем интервалы для модели
     */
    public function fillMetrics()
    {
        $metrics = json_decode($this->owner->metric);

        $this->owner->metricResultType       = $metrics->result_type ?? self::RESULT_TYPE_SINGLE;
        $this->owner->metricResultMacro      = $metrics->result_macro ?? 1;
        $this->owner->metricResultMicro      = $metrics->result_micro ?? 1;
        $this->owner->metricResultActual     = $metrics->result_actual ?? 0;
        $this->owner->metricResultExpect     = $metrics->result_expect ?? 1;
        $this->owner->metricResultAccumulate = $metrics->result_accumulate ?? 0;
    }

    /**
     * Сохраняем интервалы для модели
     */
    public function setMetrics()
    {
        $metrics = $this->getMetrics();

        $this->setResultActual($metrics);
        $this->setResultExcpect($metrics);
        $this->setResultPercent($metrics);

        $metrics['result_type']  = $this->owner->metricResultType;
        $metrics['result_macro'] = $this->owner->metricResultMacro ?: 1;
        $metrics['result_micro'] = $this->owner->metricResultMicro ?: 1;

        $this->owner->metric = json_encode($metrics);
    }

    /**
     * Считаем результат процентах
     *
     * @param array $metrics
     */
    private function setResultPercent(array &$metrics)
    {
        $allPrev = $metrics['result_actual'][TaskResult::STATUS_GOOD] + $metrics['result_actual'][TaskResult::STATUS_FAIL];
        $allNext = $metrics['result_expect'];

        $all = $allPrev + $allNext;

        $metrics['result_percent'] = 0;
        if ($metrics['result_actual'][TaskResult::STATUS_GOOD]) {
            $metrics['result_percent'] = $all / $metrics['result_actual'][TaskResult::STATUS_GOOD] * 100;
        }
    }

    /**
     * Считаем оставшиеся результаты
     *
     * @param array $metrics
     */
    private function setResultExcpect(array &$metrics)
    {
        $metrics['result_expect'] = 0;

        if ($this->owner->finished_at && $this->resultType != self::RESULT_TYPE_ACCUMULATION) {
            $results = $this->getResultsByRange(date('Y-m-d'), $this->owner->finished_at);
            /** @var TaskResult $result */
            foreach ($results as $result) {
                $metrics['result_expect']++;
            }
        }
    }


    /**
     * Считаем результаты на текущий момент
     *
     * @param array $metrics
     */
    private function setResultActual(array &$metrics)
    {
        $metrics['result_actual'] = [
            TaskResult::STATUS_NEW  => 0,
            TaskResult::STATUS_GOOD => 0,
            TaskResult::STATUS_FAIL => 0,
        ];

        if ($this->owner->started_at) {
            $results = $this->getResultsByRange($this->owner->started_at, date('Y-m-d'));
            /** @var TaskResult $result */
            foreach ($results as $result) {
                if ($this->resultType == self::RESULT_TYPE_ACCUMULATION) {
                    $metrics['result_actual'][$result->status] += $result->accumulate;
                } else {
                    $metrics['result_actual'][$result->status]++;
                }
            }
            $metrics['result_accumulate'] = $metrics['result_actual'][TaskResult::STATUS_GOOD];
        }
    }


    /**
     * Получаем параметр
     *
     * @param string|null $param
     * @return mixed|int
     */
    private function getMetrics($param = null)
    {
        if (!$this->metrics) {
            $this->metrics = json_decode($this->owner->metric, true);
        }
        if (!$param) {
            return (array)$this->metrics;
        }

        return (int)($this->metrics[$param] ?? 0);
    }


    /**
     * Получаем метрику - должно быть выполнено в день
     *
     * @return int|mixed
     */
    public function getResultAccumulate()
    {
        return $this->getMetrics('result_accumulate');
    }

    /**
     * Получаем метрику - должно быть выполнено в день
     *
     * @return int|mixed
     */
    public function getResultMicro()
    {
        return $this->getMetrics('result_micro');
    }

    /**
     * Получаем метрику - должно быть выполнено в день
     *
     * @return int|mixed
     */
    public function getResultActual()
    {
        return $this->getMetrics('result_actual');
    }

    /**
     * Получаем метрику - должно быть выполнено в день
     *
     * @return int|mixed
     */
    public function getResultExpect()
    {
        return $this->getMetrics('result_expect');
    }

    /**
     * Получаем метрику - должно быть выполнено в день
     *
     * @return int|mixed
     */
    public function getResultType()
    {
        return $this->getMetrics('result_type');
    }


    /**
     * Получаем метрику - должно быть выполнено в день
     *
     * @return int|mixed
     */
    public function getResultPercent()
    {
        return $this->getMetrics('result_percent');
    }

    /**
     * Проверяем чтозадачи за день выполнены
     *
     * @param string $date
     * @return bool
     */
    public function getDayCompleted(string $date)
    {
        $counts = $this->getDayResultsCount($date);

        return ($counts['good'] + $counts['fail']) >= $this->getMetrics('result_micro');
    }

    /**
     * Поулчаем счетчики резултатов за день
     *
     * @param string $date
     * @return array
     */
    public function getDayResultsCount(string $date, $type = null)
    {
        $counts = [
            'good' => 0,
            'fail' => 0,
        ];

        if ($results = $this->getDayResults($date)) {
            foreach ($results as $result) {
                if ($result->status === TaskResult::STATUS_GOOD) {
                    $counts['good']++;
                } elseif ($result->status === TaskResult::STATUS_FAIL) {
                    $counts['fail']++;
                } else {
                    if ($date < date('Y-m-d')) {
                        $counts['fail']++;
                    }
                }
            }
        } else {
            if ($date < date('Y-m-d')) {
                $counts['fail'] = $this->getMetrics('result_micro');
            }
        }


        if (!$type) {
            return $counts;
        }
        return $counts[$type];
    }

    /**
     * Получаем результаты за определенный день
     *
     * @param $date
     * @return array
     */
    public function getDayResults($date)
    {
        if (!$this->resultsDay) {
            /** @var TaskResult $result */
            foreach ($this->owner->results as $result) {
                if (date('Y-m-d', strtotime($result->created_at)) === date('Y-m-d', strtotime($date))) {
                    $this->resultsDay[$result->created_at] = $result;
                }
            }
            $this->resultsDay = array_reverse($this->resultsDay);
        }
        return $this->resultsDay;
    }

    /**
     * Получаем задачи за дату
     *
     * @param string   $date
     * @param int      $type
     * @param int|null $userId
     * @return Task[]
     */
    public static function getTasksByDate($date, $type, $userId = null): array
    {
        return self::getTasksByPeriodQuery($date, $date, $type, $userId)->all();
    }

    /**
     * Получаем задачи актуальные за период
     *
     * @param string       $dateStart
     * @param string       $dateEnd
     * @param integer      $type
     * @param integer|null $userId
     *
     * @return BaseActiveQuery
     */
    public static function getTasksByPeriodQuery($dateStart, $dateEnd, $type, $userId = null): BaseActiveQuery
    {
        $dates    = DateHelper::getDatesByRange($dateStart, $dateEnd, true);
        $days     = implode('\',\'', array_unique(ArrayHelper::getColumn($dates, 'day')));
        $weekdays = implode('\',\'', array_unique(ArrayHelper::getColumn($dates, 'weekday')));
        $dates    = implode('\',\'', array_keys($dates));

        $dateStart = date('Y-m-d 00:00:00', strtotime($dateStart));
        $dateEnd   = date('Y-m-d 23:59:59', strtotime($dateEnd));

        $makeTypeSingle   = Task::MAKE_TYPE_SINGLE;
        $makeTypeRepeat   = Task::MAKE_TYPE_REPEAT;
        $makeTypeContinue = Task::MAKE_TYPE_CONTINUE;

        $sql = <<<SQL
            SELECT task.* FROM unidoc_tasks_task task
            LEFT JOIN unidoc_tasks_period period ON task.id=period.object_id 
            WHERE 
             (task.make_type = {$makeTypeSingle} AND DATE(task.started_at) = DATE('{$dateStart}'))
             OR
             (
                task.make_type = {$makeTypeContinue} 
                AND task.started_at <= '{$dateEnd}'
                AND task.finished_at >= '{$dateStart}'
              )
             OR 
            (   task.make_type = {$makeTypeRepeat}
                AND (task.started_at <= '{$dateEnd}' OR task.started_at IS NULL)
                AND (task.finished_at >= '{$dateStart}' OR task.finished_at IS NULL)
                AND ( 
                 jsonb_exists_any(period.week_days :: JSONB, array['{$weekdays}'])
                 OR jsonb_exists_any(period.month_days :: JSONB, array['{$days}'])
                 OR jsonb_exists_any(period.custom_dates :: JSONB, array['{$dates}'])
                )
            ) ORDER BY time;
SQL;
        return Task::findBySql($sql)
            ->andWhere(['task_type' => $type])
            ->byCreatedBy($userId ?? Yii::$app->user->id);
    }

    /**
     * Получаем все результаты за период
     *
     * @param string $dateStart
     * @param string $dateEnd
     *
     * @return array
     */
    public function getResultsByRange(string $dateStart, string $dateEnd)
    {
        $currentResults = $this->owner->results;
        $currentByDates = [];
        foreach ($currentResults as $currentResult) {
            $currentByDates[date('Y-m-d', strtotime($currentResult->created_at))][] = $currentResult;
        }

        $dates = DateHelper::getDatesByRange($dateStart, $dateEnd);

        $results = [];
        foreach ($dates as $date) {
            if (isset($currentByDates[$date])) {
                $results = ArrayHelper::merge($results, $currentByDates[$date]);
            } else {
                $results[] = new TaskResult([
                    'status'  => (date('Y-m-d') <= $date) ? TaskResult::STATUS_NEW : TaskResult::STATUS_FAIL,
                    'task_id' => $this->owner->id,
                ]);
            }
        }

        return $results;
    }

}
