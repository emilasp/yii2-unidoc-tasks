<?php
namespace emilasp\tasks\behaviors;

use emilasp\tasks\models\Task;
use emilasp\tasks\models\TaskPeriod;
use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * Поведение добавляет функционал работы с периодами
 *
 * Class PeriodicalBehavior
 * @package emilasp\tasks\behaviors
 */
class PeriodicalBehavior extends Behavior
{
    public $monthDays;
    public $weekDays;
    public $customDates;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'savePeriod',
            ActiveRecord::EVENT_AFTER_UPDATE => 'savePeriod',
        ];
    }

    /**
     * Сохраняем интервалы для модели
     */
    public function savePeriod()
    {
        $model  = $this->owner;
        $period = $this->owner->period;

        if (!$period) {
            $period = new TaskPeriod(['object' => $model::className(), 'object_id' => $model->id]);
        }

        $period->month_days   = $this->monthDays ?: '[]';
        $period->week_days    = $this->weekDays ?: '[]';
        $period->custom_dates = $this->customDates ?: '[]';

        if ($this->monthDays || $this->weekDays || $this->customDates) {
            $period->save();
        }
    }

    /**
     * Устанавливаем интервалы
     */
    public function setPeriods()
    {
        if ($this->owner->period) {
            $this->monthDays   = $this->owner->period->month_days;
            $this->weekDays    = $this->owner->period->week_days;
            $this->customDates = $this->owner->period->custom_dates;
        }
    }

    /**
     *  Получаем задачи на определенную дату
     *
     * @param string $datetime
     * @return Task[]
     */
    public function getTasksByDate($datetime = null, $userId = null)
    {
        $class = Task::classname();
        $sql   = <<<SQL
        SELECT * FROM unidoc_tasks_task task
        INNER JOIN unidoc_tasks_period period ON period.object='{$class}' AND task.id=period.object_id
        WHERE (task.started_at <= '{$datetime}' OR task.started_at IS NULL) 
              AND (task.finished_at >= '{$datetime}' OR task.finished_at IS NULL)
              AND (
                    period.week_days @> (EXTRACT(ISODOW FROM TIMESTAMP '{$datetime}')::TEXT::JSONB)
                OR  period.month_days @> (EXTRACT(DAY FROM TIMESTAMP '{$datetime}')::TEXT::JSONB)
                /*OR  period.custom_dates @> ('{$datetime}'::TEXT::JSONB)*/
                  )
SQL;
        if ($userId) {
            $sql .= ' AND task.created_by = ' . $userId;
        }

        return Task::findBySql($sql)->with('periods')->all();
    }


    /**
     * ПРоверяем, что задача подходит под переданную дату
     *
     * @param string|null $date
     * @return bool
     */
    public function isInDate($date = null)
    {
        $this->setPeriods();

        if (!$date) {
            $date = date('Y-m-d');
        }

        $weekDay  = date('N', strtotime($date));
        $monthDay = date('j', strtotime($date));

        $weekDays    = $this->weekDays ? json_decode($this->weekDays, true) : [];
        $monthDays   = $this->monthDays ? (array)json_decode($this->monthDays, true) : [];
        $customDates = $this->customDates ? (array)json_decode($this->customDates, true) : [];

        if (in_array($weekDay, $weekDays)) {
            return true;
        }

        if (in_array($monthDay, $monthDays)) {
            return true;
        }

        foreach ($customDates as $date) {
            if (date('Y-m-d', strtotime($date)) === $date) {
                return true;
            }
        }

        return false;
    }


}
